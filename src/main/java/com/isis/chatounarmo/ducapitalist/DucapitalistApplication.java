package com.isis.chatounarmo.ducapitalist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DucapitalistApplication {

    public static void main(String[] args) {
        SpringApplication.run(DucapitalistApplication.class, args);
    }

}
