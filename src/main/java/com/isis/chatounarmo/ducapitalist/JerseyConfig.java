package com.isis.chatounarmo.ducapitalist;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/ducapitalist")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(Webservices.class);
        register(CORSResponseFilter.class);
    }
}
