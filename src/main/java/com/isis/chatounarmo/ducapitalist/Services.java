package com.isis.chatounarmo.ducapitalist;

import com.isis.chatounarmo.world.PallierType;
import com.isis.chatounarmo.world.ProductType;
import com.isis.chatounarmo.world.TyperatioType;
import com.isis.chatounarmo.world.World;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

public class Services {
    private JAXBContext jaxbContext;

    public Services() {
        try {
            jaxbContext = JAXBContext.newInstance(World.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }


    public World readWorldFromXml(String username) {
        World w = null;
        File file = new File(username + "-world.xml");
        InputStream in;

        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            in = Services.class.getClassLoader().getResourceAsStream("world.xml");
        }

        try {
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            w = (World) unmarshaller.unmarshal(in);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return w;
    }

    public void saveWorldToXML(String username, World w) {
        try {
            Marshaller marshaller = jaxbContext.createMarshaller();
            OutputStream out = new FileOutputStream(username + "-world.xml");
            marshaller.marshal(w, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public World getWorld(String username) {
        World w = readWorldFromXml(username);
        updateScore(username, w);
        saveWorldToXML(username, w);

        return w;
    }

    public boolean updateProduct(String username, ProductType newProduct) {
        World w = getWorld(username);
        ProductType productFound = null;

        if (w != null) {
            for (ProductType product : w.getProducts().getProduct()) {
                if (newProduct.getId() == product.getId()) {
                    productFound = product;
                }
            }

            if (productFound == null) {
                return false;
            }

            int qtChange = newProduct.getQuantite() - productFound.getQuantite();

            if (qtChange > 0) {
                w.setMoney(w.getMoney() - (productFound.getCout() * Math.pow(productFound.getCroissance(), productFound.getQuantite())) * (1 - Math.pow(productFound.getCroissance(), qtChange)) / (1 - productFound.getCroissance()));
                productFound.setQuantite(newProduct.getQuantite());

                for(PallierType pallier : productFound.getPalliers().getPallier()) {
                    if(productFound.getQuantite() >= pallier.getSeuil()) {
                        if(!pallier.isUnlocked()) {
                            pallier.setUnlocked(true);
                            applyPallier(pallier, productFound);
                        }
                    }
                }

                int sumProduct = 0;
                for(ProductType product : w.getProducts().getProduct()) {
                    sumProduct += product.getQuantite();
                }
                for (PallierType unlock : w.getAllunlocks().getPallier()) {
                    if(sumProduct >= unlock.getSeuil()) {
                        if(!unlock.isUnlocked()) {
                            unlock.setUnlocked(true);
                            for(ProductType product : w.getProducts().getProduct()) {
                                applyPallier(unlock, product);
                            }
                        }
                    }
                }
            } else {
                productFound.setTimeleft(productFound.getVitesse());
                w.setLastupdate(System.currentTimeMillis());
            }

            saveWorldToXML(username, w);

            return true;
        }

        return false;
    }

    public boolean updateManager(String username, PallierType newManager) {
        World w = getWorld(username);
        PallierType managerFound = null;

        for (PallierType manager : w.getManagers().getPallier()) {
            if (newManager.getName().equals(manager.getName())) {
                managerFound = manager;
            }
        }


        if (managerFound == null) {
            return false;
        }

        ProductType productFound = null;
        for (ProductType product : w.getProducts().getProduct()) {
            if (product.getId() == managerFound.getIdcible()) {
                productFound = product;
            }
        }

        productFound.setManagerUnlocked(true);
        managerFound.setUnlocked(true);
        w.setMoney(w.getMoney() - managerFound.getSeuil());

        saveWorldToXML(username, w);

        return true;
    }

    private void applyPallier(PallierType pallier, ProductType product) {
        if(pallier.getTyperatio() == TyperatioType.GAIN) {
            product.setRevenu(product.getRevenu() * pallier.getRatio());
        }
        else if(pallier.getTyperatio() == TyperatioType.VITESSE ) {
            product.setVitesse((int) (product.getVitesse() / pallier.getRatio()));
            if(product.getTimeleft() > 0) {
                product.setTimeleft((long) (product.getTimeleft() / pallier.getRatio()));
            }
        }

    }

    private void updateScore(String username, World w) {
        long now = System.currentTimeMillis();

        for (ProductType product : w.getProducts().getProduct()) {

            if (product.isManagerUnlocked()) {
                long elapsedProduct = ((now - w.getLastupdate()) - product.getTimeleft()) / product.getVitesse();
                long remainingProduct = ((now - w.getLastupdate()) - product.getTimeleft()) % product.getVitesse();

                if (product.getTimeleft() < now - w.getLastupdate()) {
                    product.setTimeleft(product.getVitesse() - remainingProduct);
                    elapsedProduct += 1;
                } else {
                    product.setTimeleft(product.getTimeleft() - (now - w.getLastupdate()));
                }

                w.setScore(w.getScore() + product.getQuantite() * product.getRevenu() * elapsedProduct);
                w.setMoney(w.getMoney() + product.getQuantite() * product.getRevenu() * elapsedProduct);

            } else {
                if (product.getTimeleft() > 0) {
                    if (product.getTimeleft() < now - w.getLastupdate()) {
                        w.setScore(w.getScore() + product.getQuantite() * product.getRevenu());
                        w.setMoney(w.getMoney() + product.getQuantite() * product.getRevenu());

                        product.setTimeleft(0);
                    } else {
                        product.setTimeleft(product.getTimeleft() - (now - w.getLastupdate()));
                    }
                }
            }
        }
        w.setLastupdate(System.currentTimeMillis());
    }
}
