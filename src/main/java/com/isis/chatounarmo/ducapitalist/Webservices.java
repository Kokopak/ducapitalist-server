package com.isis.chatounarmo.ducapitalist;

import com.google.gson.Gson;
import com.isis.chatounarmo.world.PallierType;
import com.isis.chatounarmo.world.ProductType;
import com.isis.chatounarmo.world.World;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

// The Java class will be hosted at the URI path "/helloworld"
@Path("api")
public class Webservices {
    // The Java method will process HTTP GET requests
    Services services;

    public Webservices() {
        services = new Services();
    }

    @GET
    @Path("world")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getWorld(@Context HttpServletRequest request) {
        String username = request.getHeader("X-User");
        World w = services.getWorld(username);
        return Response.ok(w).build();
    }

    @PUT
    @Path("product")
    public Response buyProduct(@Context HttpServletRequest request, String body) {
        String username = request.getHeader("X-User");
        ProductType product = new Gson().fromJson(body, ProductType.class);
        services.updateProduct(username, product);

        return Response.ok().build();
    }

    @PUT
    @Path("manager")
    public Response buyManager(@Context HttpServletRequest request, String body) {
        String username = request.getHeader("X-User");
        PallierType manager = new Gson().fromJson(body, PallierType.class);

        services.updateManager(username, manager);

        return Response.ok().build();
    }
}
